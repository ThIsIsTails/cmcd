package ru.thisistails.cmcd.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.yaml.snakeyaml.Yaml;

import lombok.extern.slf4j.Slf4j;
import ru.thisistails.cmcd.Exceptions.ConfigurationFileNotFound;

@Slf4j
public class YamlManager {

	/**
	 * Создаёт конфиг файл или передаёт его.
	 * @param requested							Путь к файлу.
	 * @return									{@link java.util.HashMap}
	 * @throws ConfigurationFileNotFound		Если файл не может быть создан.
	 */
    public static Map<String, Object> tryGetConfigFile(File requested) throws ConfigurationFileNotFound {
		HashMap<String, Object> req = null;
		try {
			Yaml yml = new Yaml();
			req = yml.load(new FileReader(requested));
		} catch(FileNotFoundException error) {
			log.error("Файла " + requested.getName() + " не существует!");
			log.error("Пытаемся создать новый.");

			try {
                log.debug("Попытка копирования...");

                InputStream stream = YamlManager.class.getResourceAsStream(requested.getName());
                
                FileUtils.copyInputStreamToFile(stream, requested);
                log.debug("Файл успешно создан.");
				return tryGetConfigFile(requested);
                
            } catch (IOException e) {
                throw new ConfigurationFileNotFound();
            }
		}

		return req;
	}

}
