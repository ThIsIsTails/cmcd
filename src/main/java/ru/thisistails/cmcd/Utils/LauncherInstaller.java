package ru.thisistails.cmcd.Utils;

import java.io.File;

import lombok.extern.slf4j.Slf4j;
import ru.thisistails.cmcd.Main;

@Slf4j
public class LauncherInstaller {
    
    /**
     * Устанавливает Forge на выбранной версии.
     * @param version       Версия загрузчика
     * @param automate      Автоматизировать установку? Если нет, то пользователь будет обязан подтвердить установку самостоятельно.
     * @param forge         Место с установщиком.
     * @param target        Куда устанавливать.
     */
    public static boolean tryInstallForge(File forge, File target, String version, boolean automate) {
        Class<?> forgeClass = null;

        log.info(Main.getBundle().getString("cli.launchingInstallerForge"));

        try {
            forgeClass = Class.forName("cpw.mods.fml.installer.ClientInstall", true, LauncherInstaller.class.getClassLoader());
        } catch (ClassNotFoundException error) {
            return false;
        }

        try {
            Object installer = forgeClass.getConstructor().newInstance();
        } catch (ReflectiveOperationException e) {
            log.error(Main.getLocaleString("cli.errors.launcher.failedToInstall"), e);
        }

        return true;
    }

}
