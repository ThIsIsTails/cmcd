package ru.thisistails.cmcd.Utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;

import org.apache.commons.io.FileUtils;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.thisistails.cmcd.Main;

@Slf4j
public class Downloader {

    private static final String forge_url = "https://maven.minecraftforge.net/net/minecraftforge/forge/{0}/forge-{0}-installer.jar";
    private static final @Getter String tmpdir = System.getProperty("java.io.tmpdir");
    
    /**
     * Скачивает установщик Forge в временную папку в системе.
     * @param version           Версия Forge.
     * @return                  Успех операции.
     */
    public static boolean installForgeInstaller(String version) {
        log.info(Main.getBundle().getString("cli.download"), "Forge Installer [" + version + "]");

        try {
            URL url = URI.create(forge_url.replace("{0}", version)).toURL();
            FileUtils.copyURLToFile(url, new File(tmpdir + "/forgeInstaller.jar"), 30000, 30000);
        } catch (IOException e) {
            log.error(Main.getBundle().getString("cli.errors.unexpected"), e);
            return false;
        }

        return true;
    }

}
