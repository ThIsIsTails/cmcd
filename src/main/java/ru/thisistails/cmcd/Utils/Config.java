package ru.thisistails.cmcd.Utils;

import java.io.File;
import java.util.Map;

import lombok.Getter;
import lombok.NonNull;

public class Config {

    private static final @Getter Config instance;

    private @Getter Map<String, Object> content;

    static {
        instance = new Config();
    }

    private Config() {
        content = YamlManager.tryGetConfigFile(new File("config.yml"));
    }

    /**
     * @param path      Для разделения пути надо использовать "/"
     * @return          String
     */
    public String getString(@NonNull String path) {
        String[] splitPath = path.split("/");

        if (splitPath.length == 0) {
            return (String) content.get(path);
        }

        String toGet = splitPath[splitPath.length - 1];
        String[] newSplitPath = new String[splitPath.length - 1];
        for (int i = 0; i < splitPath.length - 1; i++) {
            newSplitPath[i] = splitPath[i];
        }

        return (String) getMap(newSplitPath).get(toGet);
    }

    /**
     * @param path      Для разделения пути надо использовать "/"
     * @return          true/false
     */
    public boolean getBool(@NonNull String path) {
        String[] splitPath = path.split("/");

        if (splitPath.length == 0) {
            return (boolean) content.get(path);
        }

        String toGet = splitPath[splitPath.length - 1];
        String[] newSplitPath = new String[splitPath.length - 1];
        for (int i = 0; i < splitPath.length - 1; i++) {
            newSplitPath[i] = splitPath[i];
        }

        return (boolean) getMap(newSplitPath).get(toGet);
    }

    // Не самое лучшее решение, но да пох
    // UPD: Я потратил на эту функцию 20 минут
    @SuppressWarnings("unchecked")
    private Map<String, Object> getMap(@NonNull String[] path) {
        Map<String, Object> toReturn = content;
        if (path.length == 1) {
            return (Map<String, Object>) content.get(path[0]);
        }

        for (int i = 0 ; i <= path.length - 1 ; i++) {
            toReturn = (Map<String, Object>) toReturn.get(path[i]);
        }

        return toReturn;
    }

}
