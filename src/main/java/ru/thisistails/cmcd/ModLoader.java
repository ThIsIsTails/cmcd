package ru.thisistails.cmcd;

import lombok.Getter;
import lombok.NonNull;

public enum ModLoader {
    Forge("forge"),
    Fabric("fabric");
    // Поддержка Quilt и других будет позже.

    private @Getter String manifestName;

    ModLoader(@NonNull String manifestName) {
        this.manifestName = manifestName;
    }

    public static ModLoader of(@NonNull String manifestName) {
        for (ModLoader i : ModLoader.values()) {
            if (i.getManifestName() == manifestName)
                return i;
        }

        return null;
    }
}
