package ru.thisistails.cmcd.Curseforge;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.extern.slf4j.Slf4j;
import ru.thisistails.cmcd.Main;
import ru.thisistails.cmcd.ModLoader;

@Slf4j
public class ModpackInfo {

    private String version = null;
    private ModLoader loader = null;
    
    public ModpackInfo(File manifest) {
        JSONObject data = null;

        try {
            data = new JSONObject(FileUtils.readFileToString(manifest, "UTF-8"));
        } catch (JSONException | IOException e) {
            log.error(Main.getLocaleString("cli.errors.failedToReadManifest"), e);
        }

        String[] ver = data.getJSONObject("minecraft").optJSONArray("modLoaders").getJSONObject(0).getString("id").split("-");

        loader = ModLoader.of(ver[0]);
    }

}
