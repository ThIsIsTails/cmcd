package ru.thisistails.cmcd.Curseforge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

import org.json.JSONObject;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.thisistails.cmcd.Main;
import ru.thisistails.cmcd.Utils.Config;

@Slf4j
public class CurseApi {
    
    private static final @Getter String baseUrl = "https://api.curseforge.com/v1/";
    private static final @Getter String modpackQuery  = "mods/%d";
    
    /**
     * Если будет способ попроще, то можно будет заменить.
     */
    private static @Getter @Setter String token = null;

    /**
     * Проверяет есть ли и доступен ли мод на CurseForge.
     * @param modId     Айди мода или модпака.
     * @return          Доступен ли мод.
     */
    public static boolean isModValidOnCurse(int modId) {
        JSONObject data = getDataFromMod(modId);

        return data.getBoolean("isAvailable");
        
    }

    /**
     * Получает информацию о моде или модпаке.
     * @param modId                         ProjectID
     * @return                              Информация о моде. При провале возращает null
     */
    public static JSONObject getDataFromMod(int modId) {
        URL query;
        try {
            query = new URI(baseUrl + String.format(modpackQuery, modId)).toURL();
        } catch (MalformedURLException | URISyntaxException e) {
            log.error(Main.getLocaleString("cli.errors.failedToGetDataFromMod"), e);
            return null;
        }

        HttpURLConnection con;
        try {
            con = (HttpURLConnection) query.openConnection();
        } catch (IOException e) {
            log.error(Main.getLocaleString("cli.errors.failedToGetDataFromMod"), e);
            return null;
        }
        con.addRequestProperty("x-api-key", Config.getInstance().getString("apiToken"));
        try {
            con.setRequestMethod("GET");
        } catch (ProtocolException e) {
            log.error(Main.getLocaleString("cli.errors.failedToGetDataFromMod"), e);
            return null;
        }
        con.addRequestProperty("Accept", "application/json");
        try {
            con.connect();
        } catch (IOException e) {
            log.error(Main.getLocaleString("cli.errors.failedToGetDataFromMod"), e);
            return null;
        }

        int status;
        try {
            status = con.getResponseCode();
        } catch (IOException e) {
            log.error(Main.getLocaleString("cli.errors.failedToGetDataFromMod"), e);
            return null;
        }

        if (status != 200) {
            log.warn(Main.getBundle().getString("cli.warns.curseforge.status"), status);

            switch(status) {
                case 404:
                    log.error(Main.getBundle().getString("cli.errors.wrongId"));
                    return null;
                case 403:
                    log.error(Main.getBundle().getString("cli.errors.http.forbidden"));
                    return null;
                case 408:
                    log.error(Main.getBundle().getString("cli.errors.timeout"));
                    return null;
                default:
                    log.warn(Main.getBundle().getString("cli.warns.continue"));
                    Scanner scan = new Scanner(System.in);
                    String answer = scan.nextLine();
                    if (!answer.equalsIgnoreCase("y")) {
                        log.error(Main.getBundle().getString("cli.errors.keyboardInterrupt"));
                        return null;
                    }
            }
        }
        
        StringBuffer response = new StringBuffer();
        try {
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            log.error(Main.getLocaleString("cli.errors.failedToGetDataFromMod"), e);
            return null;
        }

        JSONObject data = new JSONObject(response.toString()).getJSONObject("data");

        return data;
    }

    // public static List<Integer> getModList(int projectID) {
    //     return getModList(getDataFromMod(projectID));
    // }

    // private static List<Integer> getModList(File manifest) {
    //     JSONObject data = null;

    //     try {
    //         data = new JSONObject(FileUtils.readFileToString(manifest, "UTF-8"));
    //     } catch (JSONException | IOException e) {
    //         log.error(Main.getLocaleString("cli.errors.failedToReadManifest"), e);
    //     }



    //     return null;
    // }

}
