package ru.thisistails.cmcd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import ru.thisistails.cmcd.Curseforge.CurseApi;
import ru.thisistails.cmcd.Utils.Config;

@Slf4j
public class Main {

    private static @Getter ResourceBundle bundle;

    public static String getLocaleString(@NonNull String key) {
        return bundle.getString(key);
    }

    public static void main(String[] args) throws URISyntaxException {
        Config config = Config.getInstance();
        Locale locale = Locale.of(config.getString("locale"));
        bundle = ResourceBundle.getBundle("locale.locale", locale);
        
        if (!config.getBool("checkUpdates")) {
            log.debug(getLocaleString("cli.checkUpdatesDisabled"));
        } else {
            Document doc;
            try {
                doc = Jsoup.connect("https://gitlab.com/ThIsIsTails/cmcd/-/raw/main/src/main/resources/version.txt").get();
                URL verStream = Main.class.getResource("/version.txt");
                try(BufferedReader br = new BufferedReader(new FileReader(new File(verStream.toURI())))) {
                    StringBuilder sb = new StringBuilder();
                    String line = br.readLine();

                    while (line != null) {
                        sb.append(line);
                        sb.append(System.lineSeparator());
                        line = br.readLine();
                    }
                    String everything = sb.toString();

                    ComparableVersion ver = new ComparableVersion(everything.replace("\n", ""));
                    ComparableVersion rem = new ComparableVersion(doc.text());

                    if (rem.compareTo(ver) != 0)
                        log.warn(getLocaleString("cli.updateRequired"), ver, rem);
                }
            } catch (IOException e) {
                log.error(getLocaleString("cli.errors.failedToCheckUpdates"), e);
            }
        }

        if (args.length != 0) {
            String[] arg = args[0].split("_");
            // if (!arg.matches("[0-9]*_[0-9]*")) {
            //     log.error(getLocaleString("cli.errors.wrongArgument"), arg);
            //     Runtime.getRuntime().exit(1);
            // }

            // TODO: Поиск по FileID
            int modId = -1, fileId = -1;
            
            try {
                modId = Integer.parseInt(arg[0]);
                if (arg.length == 2)
                    fileId = Integer.parseInt(arg[1]);
            } catch (NumberFormatException e) {
                log.error(getLocaleString("cli.errors.wrongArgument"), arg);
                Runtime.getRuntime().exit(1);
            }

            if (modId < 0) {
                log.error(getLocaleString("cli.errors.wrongArgument"), arg);
                Runtime.getRuntime().exit(1);
            }

            try {
                log.debug("Mod is avaliable: {}", CurseApi.isModValidOnCurse(modId));
            } catch (NumberFormatException e) {
                log.error("Something went wrong.", e);
            }
        }
        
    }

}
