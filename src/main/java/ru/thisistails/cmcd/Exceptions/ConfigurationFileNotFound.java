package ru.thisistails.cmcd.Exceptions;

/**
 * Если конфиг не найден или не может быть создан
 */
public class ConfigurationFileNotFound extends RuntimeException {}
